package org.humor.animator;

import java.util.LinkedList;

public class Animator {

	// do not manually add animations to this list
	private static LinkedList<Animator> animations = new LinkedList<Animator>();
	private static LinkedList<Animator> animatedAnimations = new LinkedList<Animator>();

	public static void updateAnimations(long time) {
		System.out.println(animations.size());
		for (Animator a : animations) {
			a.update(time);
			if (a.finished) {
				animatedAnimations.push(a);
				if (a.next != null) {
					animations.add(a.next);
				}
			}
		}

		while (!animatedAnimations.isEmpty()) {
			animations.remove(animatedAnimations.pop());
		}
	}

	public float power = 1; // 1 = linear
	public float startValue;
	public float endValue;
	public float currentValue;
	public long startTime;
	public float duration;
	public Animator next; // when the current animator is played through, it
							// removes itself from the list, adding the next
							// animator

	public boolean cyclic = false;
	public boolean finished = false;

	public Animator(float startValue, float endValue, long startTime,
			int duration, float power) {
		this.startValue = startValue;
		this.currentValue = startValue;
		this.endValue = endValue;
		this.startTime = startTime;
		this.duration = duration;
		this.power = power;
		animations.add(this);
	}

	public Animator(float startValue, float endValue, long startTime,
			int duration) {
		this(startValue, endValue, startTime, duration, 1);
	}

	// private Animator(float endValue, int duration, float power,
	// Animator previous) {
	// this.startValue = previous.endValue;
	// this.currentValue = startValue;
	// this.endValue = endValue;
	// this.duration = duration;
	// this.startTime = previous.startTime + previous.duration;
	// this.power = power;
	// }
	//
	// public void addNext(float endValue, int duration, float power) {
	// this.next = new Animator(endValue, duration, power, this);
	// }

	public void update(long time) { // animations should generally be updated
									// using the static updateAnimations method
		if (cyclic) {
			time = time % (int) duration;
		}
		if (time > startTime && time <= startTime + duration) {
			float factor = (float) Math.pow((time - startTime) / duration,
					power);
			currentValue = factor * endValue + (1 - factor) * startValue;
		} else if (time > startTime + duration) {
			finished = true;
		}

	}
}