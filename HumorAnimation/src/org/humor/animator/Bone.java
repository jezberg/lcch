package org.humor.animator;

import java.util.ArrayList;

public class Bone {

	public Bone parent;
	public ArrayList<Bone> children = new ArrayList<Bone>();
	public float segLength;
	public float angle;
	public float x = 0;
	public float y = 0;
	public boolean isHead = false;

	public Bone(float segLength, float angle, Bone parent) {
		this.parent = parent;
		parent.children.add(this);
		this.segLength = segLength;
		this.angle = angle;
		this.x = parent.segLength;
	}

	public Bone(float segLength, Bone parent) {
		this.parent = parent;
		parent.children.add(this);
		this.segLength = segLength;
		this.angle = 0;
		this.x = parent.segLength;
	}

	public Bone(float segLength, float angle) {
		this.segLength = segLength;
		this.angle = angle;
	}

	public void drawBones() {
		Main.singleton.pushMatrix(); // save parent transformation before
										// applying own transform;
		Main.singleton.translate(x, y);
		Main.singleton.rotate(angle);
		if (isHead) {
			Main.singleton.ellipse(segLength, 0, segLength, 0.8f * segLength);
		} else {
			Main.singleton.line(0, 0, segLength, 0);
		}
		for (Bone b : children) {
			b.drawBones();
		}
		Main.singleton.popMatrix();
	}
}
