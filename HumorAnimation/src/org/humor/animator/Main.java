package org.humor.animator;

import processing.core.PApplet;

public class Main extends PApplet {
	private static final long serialVersionUID = 1L;

	public static Main singleton;

	public Main() {
		System.out.println("Singleton created.");
		singleton = this;
	}

	float x, y;

	Bone spine = new Bone(120, -PI / 2);
	Bone rightArm1 = new Bone(75, 0.75f * PI, spine);
	Bone rightArm2 = new Bone(70, 0.3f * PI, rightArm1);
	Bone leftArm1 = new Bone(75, -0.75f * PI, spine);
	Bone leftArm2 = new Bone(70, -0.3f * PI, leftArm1);
	Bone rightLeg1 = new Bone(75, 0.8f * PI, spine);
	Bone rightLeg2 = new Bone(75, 0.15f * PI, rightLeg1);
	Bone leftLeg1 = new Bone(75, -0.8f * PI, spine);
	Bone leftLeg2 = new Bone(75, -0.15f * PI, leftLeg1);
	Bone head = new Bone(40, spine);

	long startTime;
	Animator wavearm1;
	Animator jump;

	@Override
	public void setup() {
		size(400, 600);
		strokeWeight(20);
		stroke(255, 255);

		x = width * 0.5f;
		y = height * 0.5f;
		noCursor();

		spine.x = x;
		spine.y = y;
		rightLeg1.x = 0; // starts at root of parent bone
		leftLeg1.x = 0; // starts at root of parent bone
		head.isHead = true;

		startTime = System.currentTimeMillis();
		wavearm1 = new Animator(leftArm1.angle, 2 * PI + leftArm1.angle, 0, 1000);
		wavearm1.cyclic = true;
		jump = new Animator(spine.y, spine.y - spine.segLength / 2, 1000, 1000,
				2);
	}

	@Override
	public void draw() {
		background(0);
		long time = System.currentTimeMillis() - startTime;
		Animator.updateAnimations(time);
		leftArm1.angle = wavearm1.currentValue;
		spine.y = jump.currentValue;
		pushMatrix();
		spine.drawBones();
		popMatrix();
	}

	// public static void main(String args[]) {
	// PApplet.main(new String[] { "--present", Main.class.getName() });
	// }
}
